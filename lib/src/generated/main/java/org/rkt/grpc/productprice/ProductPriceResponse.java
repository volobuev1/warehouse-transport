// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ProductPriceService.proto

package org.rkt.grpc.productprice;

/**
 * Protobuf type {@code org.rkt.grpc.ProductPriceResponse}
 */
public final class ProductPriceResponse extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:org.rkt.grpc.ProductPriceResponse)
    ProductPriceResponseOrBuilder {
private static final long serialVersionUID = 0L;
  // Use ProductPriceResponse.newBuilder() to construct.
  private ProductPriceResponse(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private ProductPriceResponse() {
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new ProductPriceResponse();
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private ProductPriceResponse(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new java.lang.NullPointerException();
    }
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          case 9: {

            productPrice_ = input.readDouble();
            break;
          }
          default: {
            if (!parseUnknownField(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return org.rkt.grpc.productprice.ProductPriceProto.internal_static_org_rkt_grpc_ProductPriceResponse_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return org.rkt.grpc.productprice.ProductPriceProto.internal_static_org_rkt_grpc_ProductPriceResponse_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            org.rkt.grpc.productprice.ProductPriceResponse.class, org.rkt.grpc.productprice.ProductPriceResponse.Builder.class);
  }

  public static final int PRODUCTPRICE_FIELD_NUMBER = 1;
  private double productPrice_;
  /**
   * <code>double productPrice = 1;</code>
   * @return The productPrice.
   */
  @java.lang.Override
  public double getProductPrice() {
    return productPrice_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (java.lang.Double.doubleToRawLongBits(productPrice_) != 0) {
      output.writeDouble(1, productPrice_);
    }
    unknownFields.writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (java.lang.Double.doubleToRawLongBits(productPrice_) != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeDoubleSize(1, productPrice_);
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof org.rkt.grpc.productprice.ProductPriceResponse)) {
      return super.equals(obj);
    }
    org.rkt.grpc.productprice.ProductPriceResponse other = (org.rkt.grpc.productprice.ProductPriceResponse) obj;

    if (java.lang.Double.doubleToLongBits(getProductPrice())
        != java.lang.Double.doubleToLongBits(
            other.getProductPrice())) return false;
    if (!unknownFields.equals(other.unknownFields)) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + PRODUCTPRICE_FIELD_NUMBER;
    hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
        java.lang.Double.doubleToLongBits(getProductPrice()));
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static org.rkt.grpc.productprice.ProductPriceResponse parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static org.rkt.grpc.productprice.ProductPriceResponse parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static org.rkt.grpc.productprice.ProductPriceResponse parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static org.rkt.grpc.productprice.ProductPriceResponse parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static org.rkt.grpc.productprice.ProductPriceResponse parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static org.rkt.grpc.productprice.ProductPriceResponse parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static org.rkt.grpc.productprice.ProductPriceResponse parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static org.rkt.grpc.productprice.ProductPriceResponse parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static org.rkt.grpc.productprice.ProductPriceResponse parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static org.rkt.grpc.productprice.ProductPriceResponse parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static org.rkt.grpc.productprice.ProductPriceResponse parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static org.rkt.grpc.productprice.ProductPriceResponse parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(org.rkt.grpc.productprice.ProductPriceResponse prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code org.rkt.grpc.ProductPriceResponse}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:org.rkt.grpc.ProductPriceResponse)
      org.rkt.grpc.productprice.ProductPriceResponseOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return org.rkt.grpc.productprice.ProductPriceProto.internal_static_org_rkt_grpc_ProductPriceResponse_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return org.rkt.grpc.productprice.ProductPriceProto.internal_static_org_rkt_grpc_ProductPriceResponse_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              org.rkt.grpc.productprice.ProductPriceResponse.class, org.rkt.grpc.productprice.ProductPriceResponse.Builder.class);
    }

    // Construct using org.rkt.grpc.productprice.ProductPriceResponse.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      productPrice_ = 0D;

      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return org.rkt.grpc.productprice.ProductPriceProto.internal_static_org_rkt_grpc_ProductPriceResponse_descriptor;
    }

    @java.lang.Override
    public org.rkt.grpc.productprice.ProductPriceResponse getDefaultInstanceForType() {
      return org.rkt.grpc.productprice.ProductPriceResponse.getDefaultInstance();
    }

    @java.lang.Override
    public org.rkt.grpc.productprice.ProductPriceResponse build() {
      org.rkt.grpc.productprice.ProductPriceResponse result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public org.rkt.grpc.productprice.ProductPriceResponse buildPartial() {
      org.rkt.grpc.productprice.ProductPriceResponse result = new org.rkt.grpc.productprice.ProductPriceResponse(this);
      result.productPrice_ = productPrice_;
      onBuilt();
      return result;
    }

    @java.lang.Override
    public Builder clone() {
      return super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof org.rkt.grpc.productprice.ProductPriceResponse) {
        return mergeFrom((org.rkt.grpc.productprice.ProductPriceResponse)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(org.rkt.grpc.productprice.ProductPriceResponse other) {
      if (other == org.rkt.grpc.productprice.ProductPriceResponse.getDefaultInstance()) return this;
      if (other.getProductPrice() != 0D) {
        setProductPrice(other.getProductPrice());
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      org.rkt.grpc.productprice.ProductPriceResponse parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (org.rkt.grpc.productprice.ProductPriceResponse) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private double productPrice_ ;
    /**
     * <code>double productPrice = 1;</code>
     * @return The productPrice.
     */
    @java.lang.Override
    public double getProductPrice() {
      return productPrice_;
    }
    /**
     * <code>double productPrice = 1;</code>
     * @param value The productPrice to set.
     * @return This builder for chaining.
     */
    public Builder setProductPrice(double value) {
      
      productPrice_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>double productPrice = 1;</code>
     * @return This builder for chaining.
     */
    public Builder clearProductPrice() {
      
      productPrice_ = 0D;
      onChanged();
      return this;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:org.rkt.grpc.ProductPriceResponse)
  }

  // @@protoc_insertion_point(class_scope:org.rkt.grpc.ProductPriceResponse)
  private static final org.rkt.grpc.productprice.ProductPriceResponse DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new org.rkt.grpc.productprice.ProductPriceResponse();
  }

  public static org.rkt.grpc.productprice.ProductPriceResponse getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<ProductPriceResponse>
      PARSER = new com.google.protobuf.AbstractParser<ProductPriceResponse>() {
    @java.lang.Override
    public ProductPriceResponse parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new ProductPriceResponse(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<ProductPriceResponse> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<ProductPriceResponse> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public org.rkt.grpc.productprice.ProductPriceResponse getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

