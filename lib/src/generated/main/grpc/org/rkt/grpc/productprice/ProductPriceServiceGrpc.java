package org.rkt.grpc.productprice;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.45.0)",
    comments = "Source: ProductPriceService.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class ProductPriceServiceGrpc {

  private ProductPriceServiceGrpc() {}

  public static final String SERVICE_NAME = "org.rkt.grpc.ProductPriceService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<org.rkt.grpc.productprice.ProductPriceRequest,
      org.rkt.grpc.productprice.ProductPriceResponse> getGetPriceMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getPrice",
      requestType = org.rkt.grpc.productprice.ProductPriceRequest.class,
      responseType = org.rkt.grpc.productprice.ProductPriceResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<org.rkt.grpc.productprice.ProductPriceRequest,
      org.rkt.grpc.productprice.ProductPriceResponse> getGetPriceMethod() {
    io.grpc.MethodDescriptor<org.rkt.grpc.productprice.ProductPriceRequest, org.rkt.grpc.productprice.ProductPriceResponse> getGetPriceMethod;
    if ((getGetPriceMethod = ProductPriceServiceGrpc.getGetPriceMethod) == null) {
      synchronized (ProductPriceServiceGrpc.class) {
        if ((getGetPriceMethod = ProductPriceServiceGrpc.getGetPriceMethod) == null) {
          ProductPriceServiceGrpc.getGetPriceMethod = getGetPriceMethod =
              io.grpc.MethodDescriptor.<org.rkt.grpc.productprice.ProductPriceRequest, org.rkt.grpc.productprice.ProductPriceResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getPrice"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.rkt.grpc.productprice.ProductPriceRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.rkt.grpc.productprice.ProductPriceResponse.getDefaultInstance()))
              .setSchemaDescriptor(new ProductPriceServiceMethodDescriptorSupplier("getPrice"))
              .build();
        }
      }
    }
    return getGetPriceMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ProductPriceServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<ProductPriceServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<ProductPriceServiceStub>() {
        @java.lang.Override
        public ProductPriceServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new ProductPriceServiceStub(channel, callOptions);
        }
      };
    return ProductPriceServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ProductPriceServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<ProductPriceServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<ProductPriceServiceBlockingStub>() {
        @java.lang.Override
        public ProductPriceServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new ProductPriceServiceBlockingStub(channel, callOptions);
        }
      };
    return ProductPriceServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ProductPriceServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<ProductPriceServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<ProductPriceServiceFutureStub>() {
        @java.lang.Override
        public ProductPriceServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new ProductPriceServiceFutureStub(channel, callOptions);
        }
      };
    return ProductPriceServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class ProductPriceServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void getPrice(org.rkt.grpc.productprice.ProductPriceRequest request,
        io.grpc.stub.StreamObserver<org.rkt.grpc.productprice.ProductPriceResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetPriceMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetPriceMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                org.rkt.grpc.productprice.ProductPriceRequest,
                org.rkt.grpc.productprice.ProductPriceResponse>(
                  this, METHODID_GET_PRICE)))
          .build();
    }
  }

  /**
   */
  public static final class ProductPriceServiceStub extends io.grpc.stub.AbstractAsyncStub<ProductPriceServiceStub> {
    private ProductPriceServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ProductPriceServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new ProductPriceServiceStub(channel, callOptions);
    }

    /**
     */
    public void getPrice(org.rkt.grpc.productprice.ProductPriceRequest request,
        io.grpc.stub.StreamObserver<org.rkt.grpc.productprice.ProductPriceResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetPriceMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class ProductPriceServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<ProductPriceServiceBlockingStub> {
    private ProductPriceServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ProductPriceServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new ProductPriceServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public org.rkt.grpc.productprice.ProductPriceResponse getPrice(org.rkt.grpc.productprice.ProductPriceRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetPriceMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class ProductPriceServiceFutureStub extends io.grpc.stub.AbstractFutureStub<ProductPriceServiceFutureStub> {
    private ProductPriceServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ProductPriceServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new ProductPriceServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<org.rkt.grpc.productprice.ProductPriceResponse> getPrice(
        org.rkt.grpc.productprice.ProductPriceRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetPriceMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_PRICE = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ProductPriceServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ProductPriceServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_PRICE:
          serviceImpl.getPrice((org.rkt.grpc.productprice.ProductPriceRequest) request,
              (io.grpc.stub.StreamObserver<org.rkt.grpc.productprice.ProductPriceResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ProductPriceServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ProductPriceServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return org.rkt.grpc.productprice.ProductPriceProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ProductPriceService");
    }
  }

  private static final class ProductPriceServiceFileDescriptorSupplier
      extends ProductPriceServiceBaseDescriptorSupplier {
    ProductPriceServiceFileDescriptorSupplier() {}
  }

  private static final class ProductPriceServiceMethodDescriptorSupplier
      extends ProductPriceServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ProductPriceServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ProductPriceServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ProductPriceServiceFileDescriptorSupplier())
              .addMethod(getGetPriceMethod())
              .build();
        }
      }
    }
    return result;
  }
}
