package warehouse.transport.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Унифицированная сущность для ответа")
public class ResponseDto<T> {

  @Schema(description = "Ответная сущность")
  private T data;

  @Schema(description = "Сообщения об ошибках")
  private List<String> errorList;

  public static ResponseDto<?> error(List<String> errorList) {
    return new ResponseDto<>(null, errorList);
  }

  public static <T> ResponseDto<T> success(T data) {
    return new ResponseDto<>(data, null);
  }
}
